package Models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import Mutators.CustomerListMutator;
import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class CustomerListModel 
{
	private ObservableList<CustomerListMutator> customerList;
	Connection connection;
	public CustomerListModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
		
	public boolean isDBConnected()
	{
		try
		{
			return !connection.isClosed();
		}
		catch (SQLException SQLexception)
		{
			SQLexception.printStackTrace();
				return false;
		}
	}
	
	public ObservableList<CustomerListMutator> retrieveCustomerList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "select * from customers";
		try
		{
			customerList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				customerList.add(new CustomerListMutator(resultSet.getString(2), resultSet.getString(3),
						resultSet.getString(4), resultSet.getString(5), resultSet.getString(6), 
						resultSet.getString(7), resultSet.getString(8), resultSet.getString(9)));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return customerList;
		
	}
}
