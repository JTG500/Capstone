package Models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import Mutators.InboundListMutator;
import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class InboundListModel 
{
	private ObservableList<InboundListMutator> inboundList;
	Connection connection;
	public InboundListModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
		
	public boolean isDBConnected()
	{
		try
		{
			return !connection.isClosed();
		}
		catch (SQLException SQLexception)
		{
			SQLexception.printStackTrace();
				return false;
		}
	}
	
	public ObservableList<InboundListMutator> retrieveInboundList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "SELECT DISTINCT customers.name, inbounds.*, products.item_num FROM inbounds "
					+  "INNER JOIN Customer_Inbound USING (Inbound_ID) "
					+  "INNER JOIN Customers USING (Customer_ID) "
					+  "INNER JOIN	products_Inbound USING (inbound_ID)	"
					+  "INNER JOIN products USING (product_ID)";
		try
		{
			inboundList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				inboundList.add(new InboundListMutator(resultSet.getString(1), resultSet.getString(3),
						resultSet.getString(4), resultSet.getInt(5), resultSet.getString(6), resultSet.getString(7), resultSet.getString(8)));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return inboundList;
		
	}
}
