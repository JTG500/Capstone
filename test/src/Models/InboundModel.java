package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class InboundModel 
{

	private ObservableList<String> ownerList;
	Connection connection;
	public InboundModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
	
	public ObservableList<String> retrieveOwnerList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "select DISTINCT name from Customers";
		try
		{
			ownerList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				ownerList.add(resultSet.getString(1));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return ownerList;
	}
	
	public boolean addInbound(String owner, String carrier, String shipDate, int quantity, String itemNumber, String driver, String truckNumber) throws SQLException
	{
		boolean isListed;
		boolean isCIListed = false;
		boolean isPIListed = false;
		boolean isProductListed = false;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "Select * FROM inbounds WHERE Inbound_ID IN (SELECT Inbound_ID FROM Customer_Inbound WHERE"
						  + " Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?)) AND "
						  + "ship_date = ? AND driver = ? AND truck_num = ? AND "
						  + "Inbound_ID IN (SELECT Inbound_ID FROM Products_Inbound WHERE"
						  + " Product_ID IN (SELECT product_ID FROM products WHERE item_num = ?))";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  owner);
			preparedStatement.setString(2, shipDate);
			preparedStatement.setString(3, driver);
			preparedStatement.setString(4, truckNumber);
			preparedStatement.setString(5, itemNumber);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
				isListed = true;
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			resultSet.close();
			if (isListed == false)
			{
				
				String checkPIQuery = "SELECT * FROM Products_Inbound WHERE Product_ID IN (SELECT Product_ID FROM Products WHERE item_num = ?)"
						+	"AND Inbound_ID IN (SELECT Inbound_ID FROM inbounds WHERE ship_date = ? AND driver = ? AND truck_num = ?)";
				String checkCIQuery = "SELECT * FROM Customer_Inbound WHERE Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?)"
						+	"AND inbound_ID IN (SELECT Inbound_ID FROM inbounds WHERE ship_date = ? AND driver = ? AND truck_num = ? "
						+	"AND Inbound_ID IN (SELECT Inbound_ID FROM products_Inbound WHERE Product_ID IN (SELECT Product_ID FROM products WHERE item_num = ?)))";
				String checkProductsQuery = "SELECT * FROM Products WHERE item_num = ? AND customer_ID = (SELECT Customer_ID FROM Customers WHERE name = ?)";
				try
				{
					preparedStatement = connection.prepareStatement(checkPIQuery);
					preparedStatement.setString(1,  itemNumber);
					preparedStatement.setString(2, shipDate);
					preparedStatement.setString(3, driver);
					preparedStatement.setString(4, truckNumber);
					resultSet = preparedStatement.executeQuery();
					if (resultSet.next())
						isPIListed = true;
					else
					{
						isPIListed = false;
					}
					preparedStatement.close();
					resultSet.close();
					
					preparedStatement = connection.prepareStatement(checkCIQuery);
					preparedStatement.setString(1,  owner);
					preparedStatement.setString(2, shipDate);
					preparedStatement.setString(3, driver);
					preparedStatement.setString(4, truckNumber);
					preparedStatement.setString(5, itemNumber);
					resultSet = preparedStatement.executeQuery();
					if (resultSet.next())
						isCIListed = true;
					else
					{
						isCIListed = false;
					}
					preparedStatement.close();
					resultSet.close();
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				
				preparedStatement = connection.prepareStatement(checkProductsQuery);
				preparedStatement.setString(1,  itemNumber);
				preparedStatement.setString(2, owner);
				resultSet = preparedStatement.executeQuery();
				if (resultSet.next())
					isProductListed = true;
				else
				{
					isProductListed = false;
				}
				preparedStatement.close();
				resultSet.close();
				if (isCIListed == false || isPIListed == false)
				{
					String query = "INSERT INTO inbounds (carrier, ship_date, qty, truck_num, driver) VALUES(?,?,?,?,?)";
					String addProductQuery = "INSERT INTO products (item_num, customer_ID) VALUES (?, (SELECT Customer_ID FROM Customers WHERE name = ?))";
					String addPIQuery = "INSERT INTO products_Inbound (Product_ID, Inbound_ID) VALUES ("
						+ " (SELECT product_ID FROM products WHERE item_num = ? LIMIT 1), "
						+ "(SELECT Inbound_ID from inbounds WHERE truck_num = ? AND driver = ? AND ship_date = ? LIMIT 1));";
					String addCIQuery = "INSERT INTO customer_inbound (customer_ID, inbound_ID) VALUES ("
						+ "(SELECT customer_ID FROM customers WHERE name = ? LIMIT 1), "
						+ "(SELECT Inbound_ID FROM inbounds WHERE ship_date = ? AND truck_num = ? AND driver = ?))";
					try
					{
						preparedStatement = connection.prepareStatement(query);
						preparedStatement.setString(1, carrier);
						preparedStatement.setString(2, shipDate);
						preparedStatement.setInt(3, quantity);
						preparedStatement.setString(4, truckNumber);
						preparedStatement.setString(5, driver);
						preparedStatement.execute();
						preparedStatement.close();
					}
					catch (Exception exception)
					{
						exception.printStackTrace();
					}
					if (isProductListed == false)
					{
						try
						{
							preparedStatement = connection.prepareStatement(addProductQuery);
							preparedStatement.setString(1, itemNumber);
							preparedStatement.setString(2, owner);
							preparedStatement.execute();
							preparedStatement.close();
						}
						catch (Exception exception)
						{
							exception.printStackTrace();
						}
					}
					try
					{
						preparedStatement = connection.prepareStatement(addPIQuery);
						preparedStatement.setString(1, itemNumber);
						preparedStatement.setString(2, truckNumber);
						preparedStatement.setString(3, driver);
						preparedStatement.setString(4, shipDate);
						preparedStatement.execute();
						preparedStatement.close();
					}
					catch (Exception exception)
					{
						exception.printStackTrace();
					}
					try
					{
						preparedStatement = connection.prepareStatement(addCIQuery);
						preparedStatement.setString(1, owner);
						preparedStatement.setString(2, shipDate);
						preparedStatement.setString(3, truckNumber);
						preparedStatement.setString(4, driver);
						return true;
					}
					catch (Exception exception)
					{
						exception.printStackTrace();
					}
					finally
					{
						preparedStatement.execute();
						preparedStatement.close();
					}
				}
			}
		}
			catch (Exception exception)
			{
				return false;
			}
			return false;
	}
}
