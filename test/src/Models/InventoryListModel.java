package Models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import Mutators.InventoryListMutator;
import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class InventoryListModel 
{
	private ObservableList<InventoryListMutator> inventoryList;
	Connection connection;
	public InventoryListModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
		
	public boolean isDBConnected()
	{
		try
		{
			return !connection.isClosed();
		}
		catch (SQLException SQLexception)
		{
			SQLexception.printStackTrace();
				return false;
		}
	}
	
	public ObservableList<InventoryListMutator> retrieveInventoryList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "select bins.location, bins.quantity, products.item_num, inbounds.ship_date from bins " 
					 + "left outer join bin_products using (bin_ID)"
					 + "left outer join products using (product_ID)"
					 + "left outer join products_inbound using (product_ID)"
					 + "left outer join inbounds using (inbound_ID)";
		try
		{
			inventoryList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				inventoryList.add(new InventoryListMutator(resultSet.getString(1), resultSet.getInt(2),
								resultSet.getString(3), resultSet.getString(4)));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return inventoryList;
	}
}

