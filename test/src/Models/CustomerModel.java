package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import application.DBConnector;

public class CustomerModel 
{
	Connection connection;
	public CustomerModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
		
	public boolean isDBConnected()
	{
		try
		{
			return !connection.isClosed();
		}
		catch (SQLException SQLexception)
		{
			SQLexception.printStackTrace();
				return false;
		}
	}
	
	public boolean addCustomers(String name, String address, String city, String state, String zipcode, String country, String phone, String fax) throws SQLException
	{
		boolean isListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "Select * FROM Customers WHERE name = ? and address = ? and city = ? and state = ?";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  name);
			preparedStatement.setString(2, address);
			preparedStatement.setString(3, city);
			preparedStatement.setString(4, state);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
				isListed = true;
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			if (isListed == false)
			{
				String query = "INSERT INTO Customers (Name, Address, City, State, Zipcode, Country, Phone, Fax) VALUES(?,?,?,?,?,?,?,?)";
				try
				{
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, name);
					preparedStatement.setString(2, address);
					preparedStatement.setString(3, city);
					preparedStatement.setString(4, state);
					preparedStatement.setString(5, zipcode);
					preparedStatement.setString(6, country);
					preparedStatement.setString(7, phone);
					preparedStatement.setString(8, fax);
					return false;
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.execute();
					preparedStatement.close();
				}	
			}
		}
		catch (Exception exception)
		{
			return true;
		}
		return true;
	}
	
	public boolean deleteCustomers(String name, String city, String state) throws SQLException
	{
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean isListed;
		
		String checkQuery = "Select * FROM Customers WHERE name = ? and city = ? and state = ?";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  name);
			preparedStatement.setString(2, city);
			preparedStatement.setString(3, state);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
				isListed = true;
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			if (isListed == true)
			{
				String query = "DELETE FROM Customers WHERE name = ? and city = ? and state = ?";
				try
				{
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, name);
					preparedStatement.setString(2, city);
					preparedStatement.setString(3, state);
					return true;
				}
				catch (Exception exception)
				{
					return false;
				}
				finally
				{
					preparedStatement.execute();
					preparedStatement.close();
				}
			}
		}
		catch (SQLException exception)
		{
			return false;
		}
		finally
		{
			resultSet.close();
		}
		return false;
	
	}
	
	public boolean updateCustomers(String name, String address, String city, String state, String zipcode, String country, String phone, String fax)
	{
		boolean isListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "Select * FROM Customers WHERE name = ?";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  name);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
				isListed = true;
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			if (isListed == true)
			{
				String query = "UPDATE Customers SET Address = ?, City = ?, State = ?, Zipcode = ?, Country = ?, Phone = ?, Fax = ? WHERE name = ?";
				try
				{
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, address);
					preparedStatement.setString(2, city);
					preparedStatement.setString(3, state);
					preparedStatement.setString(4, zipcode);
					preparedStatement.setString(5, country);
					preparedStatement.setString(6, phone);
					preparedStatement.setString(7, fax);
					preparedStatement.setString(8,  name);
					return true;
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.execute();
					preparedStatement.close();
				}	
			}
		}
		catch (Exception exception)
		{
			return false;
		}
		return false;
	}
}

