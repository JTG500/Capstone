package Models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import Mutators.InvoiceListMutator;
import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class InvoiceListModel 
{
	private ObservableList<InvoiceListMutator> invoiceList;
	Connection connection;
	public InvoiceListModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
		
	public ObservableList<InvoiceListMutator> retrieveInvoiceList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "SELECT Customers.name, invoices.*, outbounds.ship_date FROM invoices "
					+  "INNER JOIN Customers USING (Customer_ID) "
					+  "INNER JOIN Outbounds USING (Outbound_ID)";
		try
		{
			invoiceList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				invoiceList.add(new InvoiceListMutator(resultSet.getInt(3), resultSet.getFloat(4),
						resultSet.getFloat(5), resultSet.getString(6), resultSet.getString(1),
						resultSet.getString(7), resultSet.getString(10)));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return invoiceList;
		
	}
}
