package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class InvoiceModel 
{
	private ObservableList<String> paymentList;
	Connection connection;
	public InvoiceModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
	
	public ObservableList<String> getPaymentType()
	{
		paymentList = FXCollections.observableArrayList("Prepaid", "Cash", "Credit", "Check");
		return paymentList;
	}
	
	public boolean addInvoice(int dStored, float cpDay, float shipCost, String payType, String customer, String consignee, String date) throws SQLException
	{
		boolean isListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "SELECT invoices.* FROM invoices "
				    +		"WHERE Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?) AND "
				    +		"outbound_ID IN (SELECT outbound_ID FROM outbounds WHERE ship_date = ?)";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1, customer);
			preparedStatement.setString(2, date);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
				isListed = true;
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			if (isListed == false)
			{
				String addQuery = "INSERT INTO Invoices (Days_Stored, Storage_Cost_per_day, Shipping_Cost, Payment_Type, consignee, Outbound_ID, Customer_ID) VALUES(?,?,?,?,?,"
						+ "(SELECT Outbound_ID FROM outbounds WHERE ship_date = ? AND Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?)), "
						+ "(SELECT Customer_ID FROM customers WHERE name = ?))";
				try
				{
					preparedStatement = connection.prepareStatement(addQuery);
					preparedStatement.setInt(1, dStored);
					preparedStatement.setFloat(2, cpDay);
					preparedStatement.setFloat(3, shipCost);
					preparedStatement.setString(4, payType);
					preparedStatement.setString(5, consignee);
					preparedStatement.setString(6, date);
					preparedStatement.setString(7, customer);
					preparedStatement.setString(8, customer);
					return false;
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.execute();
					preparedStatement.close();
				}	
			}
		}
		catch (Exception exception)
		{
			return true;
		}
		return true;
	}
	
	public boolean updateInvoice(int dStored, float cpDay, float shipCost, String payType, String customer, String consignee, String date) throws SQLException
	{
		boolean isListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "SELECT invoices.* FROM invoices "
				    +		"WHERE Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?) AND "
				    +		"outbound_ID IN (SELECT outbound_ID FROM outbounds WHERE ship_date = ?)";
		
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  customer);
			preparedStatement.setString(2, date);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
				isListed = true;
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			if (isListed == true)
			{
				String query = "UPDATE Invoices SET Days_Stored = ?, Storage_Cost_per_Day = ?, Shipping_Cost = ?, Payment_Type = ? WHERE Outbound_ID IN "
						+ "(SELECT Outbound_ID FROM Outbounds WHERE ship_date = ? AND Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?)) AND "
						+ "Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?)";
				try
				{
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setInt(1, dStored);
					preparedStatement.setFloat(2, cpDay);
					preparedStatement.setFloat(3, shipCost);
					preparedStatement.setString(4, payType);
					preparedStatement.setString(5, date);
					preparedStatement.setString(6, customer);
					preparedStatement.setString(7, customer);
					return true;
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.execute();
					preparedStatement.close();
				}	
			}
		}
		catch (Exception exception)
		{
			return false;
		}
		return false;
	}
}

