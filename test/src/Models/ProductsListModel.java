package Models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import Mutators.ProductsListMutator;
import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ProductsListModel 
{
	private ObservableList<ProductsListMutator> productList;
	Connection connection;
	public ProductsListModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
	
	public ObservableList<ProductsListMutator> retrieveProductList() throws SQLException
	{
		String query = "SELECT customers.name, products.* FROM Products LEFT OUTER JOIN Customers using (Customer_ID)";
		ResultSet resultSet = null;
		try
		{
			productList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				productList.add(new ProductsListMutator(resultSet.getString(1), resultSet.getString(3),
								resultSet.getString(4), resultSet.getString(5), resultSet.getFloat(6), resultSet.getFloat(7),
								resultSet.getFloat(8), resultSet.getFloat(9), resultSet.getFloat(10)));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return productList;
	}
}

