package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import application.DBConnector;

public class LoginModel 
{
	Connection connection;
	public LoginModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
		
	public boolean isDBConnected()
	{
		try
		{
			return !connection.isClosed();
		}
		catch (SQLException SQLexception)
		{
			SQLexception.printStackTrace();
				return false;
		}
	}
	
	public boolean isLoggedIn(String user, String pass) throws SQLException
	{
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String query = "select * from users where username = ? and password = ?";
		try
		{
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, user);
			preparedStatement.setString(2, pass);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet.next())
				return true;
			else
				return false;
		}
		catch(Exception exception)
		{
			return false;
		}
		finally
		{
			preparedStatement.close();
			resultSet.close();
		}
	}
}


