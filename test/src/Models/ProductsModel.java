package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ProductsModel 
{
	private ObservableList<String> ownersList;
	Connection connection;
	public ProductsModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
	public ObservableList<String> retrieveOwnerList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "select name from customers";
		try
		{
			ownersList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				ownersList.add(resultSet.getString(1));
			}
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return ownersList;
	}
	
	public boolean addProduct(String owner, String description, String itemNumber, String container, float grossWeight, float netWeight, float cube, float tie, float high)
	{
		boolean isListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "SELECT * FROM products WHERE item_num = ? AND customer_ID IN (SELECT customer_ID from customers WHERE name = ?)";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  itemNumber);
			preparedStatement.setString(2, owner);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
			{
				isListed = true;
			}
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			resultSet.close();
			if (isListed == false)
			{
				String query = "INSERT INTO products (description, item_num, container, gross_wt, net_wt, cube, tie, high, customer_ID) VALUES"
						+ "(?,?,?,?,?,?,?,?,(SELECT Customer_ID from Customers WHERE name = ? LIMIT 1));";

				try
				{
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, description);
					preparedStatement.setString(2, itemNumber);
					preparedStatement.setString(3, container);
					preparedStatement.setFloat(4, grossWeight);
					preparedStatement.setFloat(5, netWeight);
					preparedStatement.setFloat(6, cube);
					preparedStatement.setFloat(7, tie);
					preparedStatement.setFloat(8, high);
					preparedStatement.setString(9, owner);
					return true;
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.execute();
					preparedStatement.close();
				}	
			}
		}
		catch (Exception exception)
		{
			return false;
		}
		return false;
	}
	
	public boolean editProduct(String owner, String description, String itemNumber, String container, float grossWeight, float netWeight, float cube, float tie, float high)
	{
		boolean isListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "SELECT * FROM products WHERE item_num = ? AND customer_ID IN (SELECT customer_ID from customers WHERE name = ?)";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  itemNumber);
			preparedStatement.setString(2,  owner);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
			{
				isListed = true;
			}
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			resultSet.close();
			if (isListed == true)
			{
				String updateBinQuery = "UPDATE products SET description = ?, container = ?, "
						+ "gross_wt = ?, net_wt = ?, cube = ?, tie = ?, high = ? WHERE item_num = ? "
						+ "AND Customer_ID = (Select Customer_ID from customers WHERE name = ?)";
				
				try
				{
					preparedStatement = connection.prepareStatement(updateBinQuery);
					preparedStatement.setString(1, description);
					preparedStatement.setString(2, container);
					preparedStatement.setFloat(3, grossWeight);
					preparedStatement.setFloat(4, netWeight);
					preparedStatement.setFloat(5, cube);
					preparedStatement.setFloat(6, tie);
					preparedStatement.setFloat(7, high);
					preparedStatement.setString(8, itemNumber);
					preparedStatement.setString(9, owner);
					preparedStatement.execute();
					return true;
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.close();
				}
				
			}
		}
		catch (Exception exception)
		{
			return false;
		}
		return false;
	}

	public boolean deleteProduct(String owner, String description, String itemNumber, String container, float grossWeight, float netWeight, float cube, float tie, float high) throws SQLException
	{
		boolean isListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "Select * FROM products WHERE item_num = ? AND customer_ID IN (SELECT customer_ID from customers WHERE name = ?)";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1, itemNumber);
			preparedStatement.setString(2, owner);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
			{
				isListed = true;
			}
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			resultSet.close();
			if (isListed == true)
			{
				String query = "DELETE FROM products WHERE item_num = ? AND customer_ID IN (SELECT customer_ID from customers WHERE name = ?)";
				try
				{
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, itemNumber);
					preparedStatement.setString(2, owner);
					preparedStatement.execute();
					return true;
					
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.close();
				}
			}
		}	
		catch (Exception exception)
		{
			return false;
		}
		return false;
	}
	
	
}




