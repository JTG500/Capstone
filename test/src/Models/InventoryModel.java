package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class InventoryModel 
{
	private ObservableList<String> itemList;
	Connection connection;
	public InventoryModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
	
	public ObservableList<String> retrieveItemList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "select DISTINCT item_num from products";
		try
		{
			itemList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				itemList.add(resultSet.getString(1));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return itemList;
	}
	
	public boolean addInventory(String location, int quantity, String item)
	{
		boolean isListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "Select * FROM bins WHERE location = ?";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  location);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
			{
				isListed = true;
			}
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			resultSet.close();
			if (isListed == false)
			{
				String query = "INSERT INTO Bins (location, quantity) VALUES(?,?)";
				String binProductQuery = "INSERT INTO bin_products (BinProduct_ID, bin_id, product_id) VALUES "
										+ "(NULL,"
										+ "(SELECT Bin_ID FROM bins WHERE location = ? LIMIT 1),"
										+ "(SELECT Product_ID FROM products WHERE item_num = ? LIMIT 1));";

				try
				{
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, location);
					preparedStatement.setInt(2, quantity);
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.execute();
					preparedStatement.close();
				}	
				
				try
				{
					preparedStatement = connection.prepareStatement(binProductQuery);
					preparedStatement.setString(1, location);
					preparedStatement.setString(2, item);
					return false;
				}
				catch(Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.execute();
					preparedStatement.close();
				}
			}
		}
		catch (Exception exception)
		{
			return true;
		}
		return true;
	}
	
	@SuppressWarnings("resource")
	public boolean editInventory(String location, int quantity, String item)
	{
		boolean isListed;
		boolean isBPListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "Select * FROM bins WHERE location = ?";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  location);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
			{
				isListed = true;
			}
			else
			{
				isListed = false;
				return false;
			}
			preparedStatement.close();
			resultSet.close();
			if (isListed == true)
			{
				String updateBinQuery = "UPDATE bins SET quantity = ? WHERE location = ?";
				
				try
				{
					preparedStatement = connection.prepareStatement(updateBinQuery);
					preparedStatement.setInt(1, quantity);
					preparedStatement.setString(2, location);
					preparedStatement.execute();
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.close();
				}
				String checkBPQuery = "Select * FROM Bin_Products WHERE Bin_ID IN (Select Bin_ID from Bins WHERE Bins.Location = ?)";
				
				try
				{
					preparedStatement = connection.prepareStatement(checkBPQuery);
					preparedStatement.setString(1,  location);
					resultSet = preparedStatement.executeQuery();
					if (resultSet.next())
					{
						isBPListed = true;
					}
					else
					{
						isBPListed = false;
					}
					preparedStatement.close();
					resultSet.close();
					if (isBPListed == true)
					{
						String binProductQuery ="UPDATE bin_products SET "
										+	"Product_ID = (Select product_ID from Products where Products.item_num = ?)"
										+	"WHERE Bin_ID IN (Select bin_ID from Bins Where bins.location = ?)";

						try
						{
							preparedStatement = connection.prepareStatement(binProductQuery);
							preparedStatement.setString(1, item);
							preparedStatement.setString(2, location);
							preparedStatement.execute();
							return true;
						}
						catch (Exception exception)
						{
						 exception.printStackTrace();
						}
						finally
						{
							preparedStatement.close();
						}
					}		
					else
					{
						String binProductQuery ="INSERT INTO bin_products (BinProduct_ID, bin_id, product_id) VALUES "
							+ "(NULL,"
							+ "(SELECT Bin_ID FROM bins WHERE location = ? LIMIT 1),"
							+ "(SELECT Product_ID FROM products WHERE item_num = ? LIMIT 1));";
						try
						{
							preparedStatement = connection.prepareStatement(binProductQuery);
							preparedStatement.setString(1, location);
							preparedStatement.setString(2, item);
							preparedStatement.execute();
							return true;
						}
						catch (Exception exception)
						{
							exception.printStackTrace();
						}
						finally
						{
							preparedStatement.close();
						}
					}
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
			}
		}
		catch (Exception exception)
		{
			return false;
		}
		return false;
	}
	
	@SuppressWarnings("resource")
	public boolean deleteInventory(String location, int quantity, String item) throws SQLException
	{
		boolean isListed;
		boolean isBPListed = false;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "Select * FROM bins WHERE location = ?";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  location);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
			{
				isListed = true;
			}
			else
			{
				isListed = false;
				return false;
			}
			preparedStatement.close();
			resultSet.close();
			if (isListed == true)
			{
				String checkBPQuery = "Select * FROM Bin_Products WHERE Bin_ID IN (Select Bin_ID from Bins WHERE Bins.Location = ?)";
				try
				{
					preparedStatement = connection.prepareStatement(checkBPQuery);
					preparedStatement.setString(1,  location);
					resultSet = preparedStatement.executeQuery();
					if (resultSet.next())
					{
						isBPListed = true;
					}
					else
					{
						isBPListed = false;
					}		
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.close();
					resultSet.close();
				}
		
				if (isBPListed == true)
				{
					String deleteBPQuery = "DELETE FROM bin_Products WHERE Bin_ID IN (SELECT Bin_ID FROM bins WHERE location = ?)";
					try
					{
						preparedStatement = connection.prepareStatement(deleteBPQuery);
						preparedStatement.setString(1, location);
						preparedStatement.execute();
						preparedStatement.close();
					}
					catch (Exception exception)
					{
						exception.printStackTrace();
					}
			
					String deleteBinQuery = "DELETE FROM bins WHERE location = ?";
					try
					{
						preparedStatement = connection.prepareStatement(deleteBinQuery);
						preparedStatement.setString(1, location);
						preparedStatement.execute();
						return true;
					}
					catch (Exception exception)
					{
						exception.printStackTrace();
					}
					finally
					{
						preparedStatement.close();
					}
				}
				else
				{
					String query = "DELETE FROM bins WHERE location = ?";
					try
					{
						preparedStatement = connection.prepareStatement(query);
						preparedStatement.setString(1, location);
						preparedStatement.execute();
						return true;
					}
					catch (Exception exception)
					{
						exception.printStackTrace();
					}
					finally
					{
						preparedStatement.close();
					}
				}
			}
		}
		catch (Exception exception)
		{
			return false;
		}
		return false;
	}
}


