package Models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import Mutators.OrdersListMutator;
import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class OrdersListModel 
{
	private ObservableList<OrdersListMutator> ordersList;
	Connection connection;
	public OrdersListModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
	
	public ObservableList<OrdersListMutator> retrieveOrdersList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "SELECT customers.name, products.item_num, orders.* from orders INNER JOIN product_orders using (order_ID) INNER JOIN products using (product_ID) INNER JOIN Customers using (Customer_ID)";
		try
		{
			ordersList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				ordersList.add(new OrdersListMutator(resultSet.getString(1), resultSet.getString(2),
						resultSet.getInt(4), resultSet.getString(5), resultSet.getString(6), 
						resultSet.getInt(3)));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return ordersList;
		
	}
}


