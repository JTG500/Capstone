package Models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class OutboundModel 
{
	
	private ObservableList<String> itemList;
	private ObservableList<String> ownerList;
	Connection connection;
	public OutboundModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
	
	
	
	public ObservableList<String> retrieveItemList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "select DISTINCT item_num from products";
		try
		{
			itemList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				itemList.add(resultSet.getString(1));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return itemList;
	}
	
	public ObservableList<String> retrieveOwnerList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "select DISTINCT name from Customers";
		try
		{
			ownerList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				ownerList.add(resultSet.getString(1));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return ownerList;
	}
	
	
	public boolean addOutbound(String owner, String itemNumber, int quantity, String orderDate, String shipDate, String driver, String truckNumber)
	{
		boolean isListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "SELECT outbounds.* FROM Outbounds "
						+	"WHERE ship_Date = ? AND driver = ? AND Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?)";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  shipDate);
			preparedStatement.setString(2,  driver);
			preparedStatement.setString(3,  owner);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
			{
				isListed = true;
			}
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			resultSet.close();
			if (isListed == false)		
			{
				String query = "INSERT INTO Outbounds (ship_date, driver, truck_num, quantity, Customer_ID) VALUES(?,?,?,?, "
						+ "(SELECT Customer_ID FROM Customers WHERE Name = ?))";
				String addOrdersQuery = "INSERT INTO Orders (Outbound_ID) VALUES ((SELECT Outbound_ID FROM Outbounds WHERE Ship_date = ? "
						+	"AND driver = ? AND Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?)))";
				String addInvoicesQuery = "INSERT INTO Invoices (Outbound_ID) VALUES ((SELECT Outbound_ID FROM Outbounds WHERE Ship_Date = ? "
						+	"AND driver = ? AND Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?)))";
				try
				{
					preparedStatement = connection.prepareStatement(query);
					preparedStatement.setString(1, shipDate);
					preparedStatement.setString(2, driver);
					preparedStatement.setString(3, truckNumber);
					preparedStatement.setInt(4, quantity);
					preparedStatement.setString(5, owner);
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				preparedStatement.execute();
				preparedStatement.close();
				try
				{
					preparedStatement = connection.prepareStatement(addOrdersQuery);
					preparedStatement.setString(1, shipDate);
					preparedStatement.setString(2, driver);
					preparedStatement.setString(3, owner);
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				preparedStatement.execute();
				preparedStatement.close();
				try
				{
					preparedStatement = connection.prepareStatement(addInvoicesQuery);
					preparedStatement.setString(1, shipDate);
					preparedStatement.setString(2, driver);
					preparedStatement.setString(3, owner);
					return false;
				}
				catch (Exception exception)
				{
					exception.printStackTrace();
				}
				finally
				{
					preparedStatement.execute();
					preparedStatement.close();
				}
			}
		}
		catch(Exception exception)
		{
			return true;
		}
		return true;
	}

	public boolean updateOutbound(String owner, String itemNumber, int quantity, String orderDate, String shipDate, String driver, String truckNumber)
	{
		boolean isListed;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		String checkQuery = "SELECT outbounds.* FROM Outbounds "
				+	"WHERE ship_Date = ? AND driver = ? AND Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?)";
		try
		{
			preparedStatement = connection.prepareStatement(checkQuery);
			preparedStatement.setString(1,  shipDate);
			preparedStatement.setString(2,  driver);
			preparedStatement.setString(3,  owner);
			resultSet = preparedStatement.executeQuery();
			if (resultSet.next())
			{
				isListed = true;
			}
			else
			{
				isListed = false;
			}
			preparedStatement.close();
			resultSet.close();
				if (isListed == false)		
				{
					String query = "UPDATE Outbounds SET driver = ?, truck_num = ?, quantity = ? "
							+ "WHERE Customer_ID = Customer_ID IN (SELECT Customer_ID FROM Customers WHERE name = ?)";
							
					try
					{
						preparedStatement = connection.prepareStatement(query);
						preparedStatement.setString(1, driver);
						preparedStatement.setString(2, truckNumber);
						preparedStatement.setInt(3, quantity);
						preparedStatement.setString(4, owner);
						return true;
					}
					catch (Exception exception)
					{
						exception.printStackTrace();
					}
					finally
					{
						preparedStatement.execute();
						preparedStatement.close();
					}	
				}
		}
		catch (Exception exception)
		{
			return false;
		}
		return false;
	}
}
