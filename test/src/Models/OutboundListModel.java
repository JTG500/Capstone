package Models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import Mutators.OutboundListMutator;
import application.DBConnector;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class OutboundListModel 
{
	private ObservableList<OutboundListMutator> outboundList;
	Connection connection;
	public OutboundListModel() 
	{
		connection = DBConnector.Connector();
		if (connection == null)
		{
			System.out.println("Connection not established");
			System.exit(1);
		}
	}
		
	public boolean isDBConnected()
	{
		try
		{
			return !connection.isClosed();
		}
		catch (SQLException SQLexception)
		{
			SQLexception.printStackTrace();
				return false;
		}
	}
	
	public ObservableList<OutboundListMutator> retrieveOutboundList() throws SQLException
	{
		ResultSet resultSet = null;
		String query = "SELECT customers.name, products.item_num, outbounds.*, orders.Date_Ordered FROM outbounds "
					+  "LEFT OUTER JOIN Orders USING (Outbound_ID)"
					+  "INNER JOIN Customers USING (Customer_ID) "
					+  "LEFT OUTER JOIN Product_Orders USING (Order_ID) "
					+  "LEFT OUTER JOIN Products USING (Product_ID)";
		try
		{
			outboundList = FXCollections.observableArrayList();
			resultSet = connection.createStatement().executeQuery(query);
			while(resultSet.next())
			{
				outboundList.add(new OutboundListMutator(resultSet.getString(1), resultSet.getString(2),
						resultSet.getInt(7), resultSet.getString(9), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6)));
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		resultSet.close();
		return outboundList;
		
	}
}
