package application;

import java.sql.DriverManager;
import java.sql.Connection;

public class DBConnector 
{
	public static Connection Connector() 
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			Connection connection = DriverManager.getConnection("jdbc:sqlite:Fowlers.sqlite3");
			return connection;
		}
		catch (Exception exception)
		{
			return null;
		}
	}
}
