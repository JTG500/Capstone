package Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class OrdersController implements Initializable
{
	@FXML
	private Button closeButton;
	
	@FXML
	private Button listButton;
	
	@FXML
	private Button addButton;
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		
	
	}
	
	public void closePressed(ActionEvent event)
	{
		try
		{
			((Node)event.getSource()).getScene().getWindow().hide();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void addPressed(ActionEvent event)
	{
		try
		{
			
		}
		catch(Exception exception)
		{
			
		}
	}
	
	public void listPressed(ActionEvent event)
	{
		try
		{
			Stage mainStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/FXML/OrdersList.fxml").openStream());
		 	Scene scene = new Scene(root);
		 	mainStage.setScene(scene);
		 	mainStage.show();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
}
