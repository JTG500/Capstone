package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.ProductsModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ProductsController implements Initializable
{

	ProductsModel productsmodel = new ProductsModel();
	
	@FXML
	private Button closeButton;
	
	@FXML
	private Button addButton;
	
	@FXML
	private Button editButton;
	
	@FXML
	private Button removeButton;
	
	@FXML
	private Button listButton;
	
	@FXML
	private Button lookupByDescription;
	
	@FXML
	private Button lookupByCustomer;
	
	@FXML
	private TextArea descriptionBox;
	
	@FXML
	private TextField itemField;
	
	@FXML
	private TextField containerField;
	
	@FXML
	private TextField gweightField;
	
	@FXML
	private TextField cubeField;
	
	@FXML
	private TextField tieField;
	
	@FXML
	private TextField highField;
	
	@FXML
	private TextField nweightField;
	
	@FXML
	private ComboBox<String> ownerBox;
	
	@FXML
	private RadioButton none;
	
	@FXML
	private RadioButton desc;
	
	@FXML
	private RadioButton cust;
	
	private ObservableList<String> ownerList;
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		try 
		{
			ownerBox.getItems().clear();
			ownerList = productsmodel.retrieveOwnerList();
			ownerBox.setItems(ownerList);
		} 
		catch (SQLException exception) 
		{
			exception.printStackTrace();
		}
		
	}
	
	public void closePressed(ActionEvent event)
	{
		try
		{
			((Node)event.getSource()).getScene().getWindow().hide();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void addPressed(ActionEvent event)
	{
		try
		{
			if (gweightField.getText().equals("") == true)
			{
				gweightField.setText("0");
			}
			
			if (nweightField.getText().equals("") == true)
			{
				nweightField.setText("0");
			}
			
			if (cubeField.getText().equals("") == true)
			{
				cubeField.setText("0");
			}
			
			if (tieField.getText().equals("") == true)
			{
				tieField.setText("0");
			}
			
			if (tieField.getText().equals("") == true)
			{
				tieField.setText("0");
			}
			
			if (highField.getText().equals("") == true)
			{
				highField.setText("0");
			}
			
			
			if (productsmodel.addProduct(ownerBox.getValue(), descriptionBox.getText(), itemField.getText(), containerField.getText(), 
					Float.parseFloat(gweightField.getText()), Float.parseFloat(nweightField.getText()), Float.parseFloat(cubeField.getText()), 
					Float.parseFloat(tieField.getText()), Float.parseFloat(highField.getText())) == true)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void editPressed(ActionEvent event)
	{
		try
		{

			if (gweightField.getText().equals("") == true)
			{
				gweightField.setText("0");
			}
			
			if (nweightField.getText().equals("") == true)
			{
				nweightField.setText("0");
			}
			
			if (cubeField.getText().equals("") == true)
			{
				cubeField.setText("0");
			}
			
			if (tieField.getText().equals("") == true)
			{
				tieField.setText("0");
			}
			
			if (tieField.getText().equals("") == true)
			{
				tieField.setText("0");
			}
			
			if (highField.getText().equals("") == true)
			{
				highField.setText("0");
			}
			
			
			if (productsmodel.editProduct(ownerBox.getValue(), descriptionBox.getText(), itemField.getText(), containerField.getText(), 
					Float.parseFloat(gweightField.getText()), Float.parseFloat(nweightField.getText()), Float.parseFloat(cubeField.getText()), 
					Float.parseFloat(tieField.getText()), Float.parseFloat(highField.getText())) == true)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void removePressed(ActionEvent event)
	{
		try
		{
			if (gweightField.getText().equals("") == true)
			{
				gweightField.setText("0");
			}
			
			if (nweightField.getText().equals("") == true)
			{
				nweightField.setText("0");
			}
			
			if (cubeField.getText().equals("") == true)
			{
				cubeField.setText("0");
			}
			
			if (tieField.getText().equals("") == true)
			{
				tieField.setText("0");
			}
			
			if (tieField.getText().equals("") == true)
			{
				tieField.setText("0");
			}
			
			if (highField.getText().equals("") == true)
			{
				highField.setText("0");
			}
			
			
			if (productsmodel.deleteProduct(ownerBox.getValue(), descriptionBox.getText(), itemField.getText(), containerField.getText(), 
					Float.parseFloat(gweightField.getText()), Float.parseFloat(nweightField.getText()), Float.parseFloat(cubeField.getText()), 
					Float.parseFloat(tieField.getText()), Float.parseFloat(highField.getText())) == true)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void listPressed(ActionEvent event)
	{
		try
		{
			Stage mainStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/FXML/ProductsList.fxml").openStream());
	 		Scene scene = new Scene(root);
	 		mainStage.setScene(scene);
	 		mainStage.show();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
}
