package Controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;

public class SuccessController implements Initializable
{

	@FXML
	private Button okButton;
	
	public void initialize(URL location, ResourceBundle resources)
	{
		
	
	}
	
	public void okPressed(ActionEvent event)
	{
		try
		{
			((Node)event.getSource()).getScene().getWindow().hide();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
}

