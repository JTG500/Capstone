package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.InvoiceListModel;
import Mutators.InvoiceListMutator;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class InvoiceListController implements Initializable
{
	InvoiceListModel invoicelistmodel = new InvoiceListModel();
	
	
	@FXML
	private TableView<InvoiceListMutator> invoiceTable;
	
	@FXML
	private TableColumn<InvoiceListMutator, Integer> cDaysStored;
	
	@FXML
	private TableColumn<InvoiceListMutator, Float> cStorageCostPerDay;
	
	@FXML
	private TableColumn<InvoiceListMutator, Float> cShippingCost;
	
	@FXML
	private TableColumn<InvoiceListMutator, String> cPaymentType;
	
	@FXML
	private TableColumn<InvoiceListMutator, String> cCustomer;
	
	@FXML
	private TableColumn<InvoiceListMutator, String> cConsignee;
	
	@FXML
	private TableColumn<InvoiceListMutator, String> cDate;
	
	private ObservableList<InvoiceListMutator> invoiceList;
	
	public void initialize(URL location, ResourceBundle resources)
	{
		try
		{
			invoiceList = invoicelistmodel.retrieveInvoiceList();
			cDaysStored.setCellValueFactory(new PropertyValueFactory<> ("daysStored"));
			cStorageCostPerDay.setCellValueFactory(new PropertyValueFactory<> ("storageCostPerDay"));
			cShippingCost.setCellValueFactory(new PropertyValueFactory<> ("shippingCost"));
			cPaymentType.setCellValueFactory(new PropertyValueFactory<> ("paymentType"));
			cCustomer.setCellValueFactory(new PropertyValueFactory<> ("customer"));
			cConsignee.setCellValueFactory(new PropertyValueFactory<> ("consignee"));
			cDate.setCellValueFactory(new PropertyValueFactory<> ("date"));
			
			invoiceTable.setItems(null);
			invoiceTable.setItems(invoiceList);
		
		}
		catch (SQLException exception)
		{
			exception.printStackTrace();
		}
	}	
}
