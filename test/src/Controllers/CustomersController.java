package Controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.CustomerModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class CustomersController implements Initializable
{
	
	CustomerModel cModel = new CustomerModel();
	
	@FXML
	private Label confirmation;
	
	@FXML
	private Button closeButton;
	
	@FXML
	private Button addButton;
	
	@FXML
	private Button editButton;
	
	@FXML
	private Button removeButton;
	
	@FXML
	private Button listButton;
	
	@FXML
	private TextField nameField;
	
	@FXML
	private TextField addressField;
	
	@FXML
	private TextField cityField;
	
	@FXML
	private TextField stateField;
	
	@FXML
	private TextField phoneField;
	
	@FXML
	private TextField countryField;
	
	@FXML
	private TextField faxField;
	
	@FXML
	private TextField zipcodeField;
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		
	
	}
	
	public void closePressed(ActionEvent event)
	{
		try
		{
			((Node)event.getSource()).getScene().getWindow().hide();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void addPressed(ActionEvent event)
	{
		try
		{
			if (cModel.addCustomers(nameField.getText(), addressField.getText(), cityField.getText(), stateField.getText(), zipcodeField.getText(), countryField.getText(), phoneField.getText(), faxField.getText()) == false)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		
		}
		catch(SQLException exception)
		{
			exception.printStackTrace();
		}
		catch(IOException exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void editPressed(ActionEvent event)
	{
		try
		{
			if (cModel.updateCustomers(nameField.getText(), addressField.getText(), cityField.getText(), stateField.getText(), zipcodeField.getText(), countryField.getText(), phoneField.getText(), faxField.getText()) == true)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void removePressed(ActionEvent event)
	{
		try
		{
			if (cModel.deleteCustomers(nameField.getText(), cityField.getText(), stateField.getText()) == true)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
		 		Scene scene = new Scene(root);
		 		mainStage.setScene(scene);
		 		mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(SQLException exception)
		{
			confirmation.setText("Could not find customer! Please Check Spelling and try again!");
		}
		catch(IOException exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void listPressed(ActionEvent event)
	{
		try
		{
			Stage mainStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/FXML/CustomersList.fxml").openStream());
	 		Scene scene = new Scene(root);
	 		mainStage.setScene(scene);
	 		mainStage.show();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
}