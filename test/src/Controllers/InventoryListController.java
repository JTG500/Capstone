package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.InventoryListModel;
import Mutators.InventoryListMutator;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class InventoryListController implements Initializable
{
	InventoryListModel inventorylistmodel = new InventoryListModel();
	
	@FXML
	private TableView<InventoryListMutator> inventoryTable;
	
	@FXML
	private TableColumn<InventoryListMutator, String> cLocation;
	
	@FXML
	private TableColumn<InventoryListMutator, Integer> cQuantity;
	
	@FXML
	private TableColumn<InventoryListMutator, String> cItemNumber;
	
	@FXML
	private TableColumn<InventoryListMutator, String> cDateReceived;
	
	private ObservableList<InventoryListMutator> inventoryList;
	
	public void initialize(URL location, ResourceBundle resources)
	{
		try
		{
			inventoryList = inventorylistmodel.retrieveInventoryList();
			cLocation.setCellValueFactory(new PropertyValueFactory<> ("location"));
			cQuantity.setCellValueFactory(new PropertyValueFactory<> ("quantity"));
			cItemNumber.setCellValueFactory(new PropertyValueFactory<> ("itemNumber"));
			cDateReceived.setCellValueFactory(new PropertyValueFactory<> ("shipDate"));
			
			inventoryTable.setItems(null);
			inventoryTable.setItems(inventoryList);
		
		}
		catch (SQLException exception)
		{
			exception.printStackTrace();
		}
	}
}
