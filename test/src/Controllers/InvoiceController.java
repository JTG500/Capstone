package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import Models.InboundModel;
import Models.InvoiceModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class InvoiceController implements Initializable
{
	
	InboundModel inboundmodel = new InboundModel();
	InvoiceModel invoicemodel = new InvoiceModel();

	@FXML
	private Button closeButton;
	
	@FXML
	private Button addButton;
	
	@FXML
	private Button editButton;
	
	@FXML
	private Button removeButton;
	
	@FXML
	private Button listButton;
	
	@FXML
	private TextField daysstoredField;
	
	@FXML
	private TextField costpdayField;
	
	@FXML
	private TextField shippingcostField;
	
	@FXML
	private ComboBox<String> paymentBox;
	
	@FXML
	private ComboBox<String> customerBox;
	
	@FXML
	private TextField consigneeField;
	
	@FXML
	private TextField dateField;
	
	private ObservableList<String> customerList;
	
	private ObservableList<String> paymentTypeList;
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		try 
		{
			customerBox.getItems().clear();
			customerList = inboundmodel.retrieveOwnerList();
			customerBox.setItems(customerList);
			
			paymentBox.getItems().clear();
			paymentTypeList = invoicemodel.getPaymentType();
			paymentBox.setItems(paymentTypeList);
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void closePressed(ActionEvent event)
	{
		try
		{
			((Node)event.getSource()).getScene().getWindow().hide();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void addPressed(ActionEvent event)
	{
		try
		{
			if (daysstoredField.getText().equals("") == true)
			{
				daysstoredField.setText("0");
			}
			
			if (costpdayField.getText().equals("") == true)
			{
				costpdayField.setText("0");
			}
			
			if (shippingcostField.getText().equals("") == true)
			{
				shippingcostField.setText("0");
			}
			
			if (invoicemodel.addInvoice(Integer.parseInt(daysstoredField.getText()), Float.parseFloat(costpdayField.getText()), Float.parseFloat(shippingcostField.getText()), paymentBox.getValue(), customerBox.getValue(), consigneeField.getText(), dateField.getText()) == false)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void editPressed(ActionEvent event)
	{
		try
		{
			if (daysstoredField.getText().equals("") == true)
			{
				daysstoredField.setText("0");
			}
			
			if (costpdayField.getText().equals("") == true)
			{
				costpdayField.setText("0");
			}
			
			if (shippingcostField.getText().equals("") == true)
			{
				shippingcostField.setText("0");
			}
			
			if (invoicemodel.updateInvoice(Integer.parseInt(daysstoredField.getText()), Float.parseFloat(costpdayField.getText()), Float.parseFloat(shippingcostField.getText()), paymentBox.getValue(), customerBox.getValue(), consigneeField.getText(), dateField.getText()) == true)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void listPressed(ActionEvent event)
	{
		try
		{
			Stage mainStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/FXML/InvoiceList.fxml").openStream());
	 		Scene scene = new Scene(root);
	 		mainStage.setScene(scene);
	 		mainStage.show();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
}