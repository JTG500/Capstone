package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.InboundModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class InboundController implements Initializable
{
	InboundModel inboundmodel = new InboundModel();
	
	@FXML
	private Button closeButton;
	
	@FXML
	private Button addButton;
	
	@FXML
	private Button listButton;
	
	@FXML
	private TextField itemField;
	
	@FXML 
	private ComboBox<String> ownerBox;
	
	@FXML
	private TextField carrierField;
	
	@FXML
	private TextField shipdateField;
	
	@FXML
	private TextField quantityField;
	
	@FXML
	private TextField truckNumberField;
	
	@FXML
	private TextField driverField;
	
	private ObservableList<String> ownerList;
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		try 
		{
			ownerBox.getItems().clear();
			ownerList = inboundmodel.retrieveOwnerList();
			ownerBox.setItems(ownerList);
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void closePressed(ActionEvent event)
	{
		try
		{
			((Node)event.getSource()).getScene().getWindow().hide();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void addPressed(ActionEvent event)
	{
		try
		{
			if (carrierField.getText().isEmpty() == true || shipdateField.getText().isEmpty() == true || quantityField.getText().isEmpty() == true || itemField.getText().isEmpty() == true ||
				driverField.getText().isEmpty() == true || truckNumberField.getText().isEmpty() == true)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Blank.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				if (inboundmodel.addInbound(ownerBox.getValue(), carrierField.getText(), shipdateField.getText(), Integer.parseInt(quantityField.getText()), itemField.getText(),
						driverField.getText(), truckNumberField.getText()) == true)
				{
					Stage mainStage = new Stage();
					FXMLLoader loader = new FXMLLoader();
					Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
					Scene scene = new Scene(root);
					mainStage.setScene(scene);
					mainStage.show();
				}
				else
				{
					Stage mainStage = new Stage();
					FXMLLoader loader = new FXMLLoader();
					Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
					Scene scene = new Scene(root);
					mainStage.setScene(scene);
					mainStage.show();
				}
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void listPressed(ActionEvent event)
	{
		try
		{
			Stage mainStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/FXML/InboundList.fxml").openStream());
	 		Scene scene = new Scene(root);
	 		mainStage.setScene(scene);
	 		mainStage.show();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
}