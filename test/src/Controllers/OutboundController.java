package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.OutboundModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class OutboundController implements Initializable
{
	
	OutboundModel outboundmodel = new OutboundModel();
	
	@FXML
	private Button closeButton;
	
	@FXML
	private Button addButton;
	
	@FXML
	private Button editButton;
	
	@FXML
	private Button removeButton;
	
	@FXML 
	private Button listButton;
	
	@FXML
	private TextField driverField;
	
	@FXML
	private TextField quantityField;
	
	@FXML
	private TextField truckNumberField;
	
	@FXML
	private ComboBox<String> ownerBox;
	
	@FXML
	private ComboBox<String> itemBox;
	
	@FXML
	private TextField shipDateField;
	
	@FXML
	private TextField orderField;
	
	private ObservableList<String> itemList;
	
	private ObservableList<String> ownerList;
	
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		try 
		{
			itemBox.getItems().clear();
			itemList = outboundmodel.retrieveItemList();
			itemBox.setItems(itemList);
			ownerBox.getItems().clear();
			ownerList = outboundmodel.retrieveOwnerList();
			ownerBox.setItems(ownerList);
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	
	}
	
	public void closePressed(ActionEvent event)
	{
		try
		{
			((Node)event.getSource()).getScene().getWindow().hide();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void addPressed(ActionEvent event)
	{
		try
		{
			if (quantityField.getText().equals("") == true)
			{
				quantityField.setText("0");
			}
			
			if (outboundmodel.addOutbound(ownerBox.getValue(), itemBox.getValue(), Integer.parseInt(quantityField.getText()), orderField.getText(), shipDateField.getText(), driverField.getText(), truckNumberField.getText()) == false)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void editPressed(ActionEvent event)
	{
		try
		{
			if (quantityField.getText().equals("") == true)
			{
				quantityField.setText("0");
			}
			
			if (outboundmodel.updateOutbound(ownerBox.getValue(), itemBox.getValue(), Integer.parseInt(quantityField.getText()), orderField.getText(), shipDateField.getText(), driverField.getText(), truckNumberField.getText()) == true)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	
	public void listPressed(ActionEvent event)
	{
		try
		{
			Stage mainStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/FXML/OutboundList.fxml").openStream());
	 		Scene scene = new Scene(root);
	 		mainStage.setScene(scene);
	 		mainStage.show();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
}