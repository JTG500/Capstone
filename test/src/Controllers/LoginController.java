package Controllers;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import Models.LoginModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class LoginController implements Initializable 
{
	LoginModel loginModel = new LoginModel();
			 
	@FXML
	private Label isConnected;
	
	@FXML
	private TextField usernameField;
	
	@FXML
	private PasswordField passwordField;
			 
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		if (loginModel.isDBConnected()) 
		{
			isConnected.setText("Connected");
		} 
		else 
		{
			isConnected.setText("Not Connected");
		}
	}
	
	public void Login(ActionEvent event)
	{
		try 
		{
			if (loginModel.isLoggedIn(usernameField.getText(), passwordField.getText()))
			{
				isConnected.setStyle("-fx-font: 18 regular");
				isConnected.setTextFill(Color.GREEN);
				isConnected.setText("Username and Password is correct!");
				((Node)event.getSource()).getScene().getWindow().hide();
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				GridPane root = loader.load(getClass().getResource("/FXML/Home.fxml").openStream());
		 		Scene scene = new Scene(root);
		 		mainStage.setScene(scene);
		 		mainStage.show();
			}
			else
			{
				isConnected.setStyle("-fx-font: 18 regular");
				isConnected.setTextFill(Color.RED);
				isConnected.setText("Wrong username and/or password! Please try again");
			}
		} 
		catch (SQLException exception) 
		{
			isConnected.setStyle("-fx-font: 18 regular");
			isConnected.setTextFill(Color.RED);
			isConnected.setText("Wrong username and/or password! Please try again");
			exception.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}


