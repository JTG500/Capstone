package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.CustomerListModel;
import Mutators.CustomerListMutator;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class CustomersListController implements Initializable
{
	CustomerListModel customerlistmodel = new CustomerListModel();
	
	@FXML
	private Label isConnected;
	
	@FXML
	private TableView<CustomerListMutator> customerTable;
	
	@FXML
	private TableColumn<CustomerListMutator, String> cName;
	
	@FXML
	private TableColumn<CustomerListMutator, String> cAddress;
	
	@FXML
	private TableColumn<CustomerListMutator, String> cCity;
	
	@FXML
	private TableColumn<CustomerListMutator, String> cState;
	
	@FXML
	private TableColumn<CustomerListMutator, String> cZipcode;
	
	@FXML
	private TableColumn<CustomerListMutator, String> cCountry;
	
	@FXML
	private TableColumn<CustomerListMutator, String> cPhone;
	
	@FXML
	private TableColumn<CustomerListMutator, String> cFax;
	
	private ObservableList<CustomerListMutator> customerList;
	
	public void initialize(URL location, ResourceBundle resources)
	{
		if (customerlistmodel.isDBConnected()) 
		{
			isConnected.setText("Connected");
		} 
		else 
		{
			isConnected.setText("Not Connected");
		}
		
		try
		{
			customerList = customerlistmodel.retrieveCustomerList();
			cName.setCellValueFactory(new PropertyValueFactory<> ("Name"));
			cAddress.setCellValueFactory(new PropertyValueFactory<> ("address"));
			cCity.setCellValueFactory(new PropertyValueFactory<> ("city"));
			cState.setCellValueFactory(new PropertyValueFactory<> ("state"));
			cZipcode.setCellValueFactory(new PropertyValueFactory<> ("zipcode"));
			cCountry.setCellValueFactory(new PropertyValueFactory<> ("country"));
			cPhone.setCellValueFactory(new PropertyValueFactory<> ("phone"));
			cFax.setCellValueFactory(new PropertyValueFactory<> ("fax"));
			customerTable.setItems(null);
			customerTable.setItems(customerList);
		
		}
		catch (SQLException exception)
		{
			exception.printStackTrace();
		}
	}	
}
