package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.ProductsListModel;
import Mutators.ProductsListMutator;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ProductsListController implements Initializable
{
	ProductsListModel productlistmodel = new ProductsListModel();
	
	@FXML
	private TableView<ProductsListMutator> productsTable;
	
	@FXML
	private TableColumn<ProductsListMutator, String> cOwner;
	
	@FXML
	private TableColumn<ProductsListMutator, String> cDescription;
	
	@FXML
	private TableColumn<ProductsListMutator, String> cItemNumber;
	
	@FXML
	private TableColumn<ProductsListMutator, String> cContainer;
	
	@FXML
	private TableColumn<ProductsListMutator, Float> cGrossWeight;
	
	@FXML
	private TableColumn<ProductsListMutator, Float> cNetWeight;
	
	@FXML
	private TableColumn<ProductsListMutator, Float> cCube;
	
	@FXML
	private TableColumn<ProductsListMutator, Float> cTie;
	
	@FXML
	private TableColumn<ProductsListMutator, Float> cHigh;
	
	private ObservableList<ProductsListMutator> productList;
	
	public void initialize(URL location, ResourceBundle resources)
	{
		try
		{
			productList = productlistmodel.retrieveProductList();
			cOwner.setCellValueFactory(new PropertyValueFactory<> ("owner"));
			cDescription.setCellValueFactory(new PropertyValueFactory<> ("description"));
			cItemNumber.setCellValueFactory(new PropertyValueFactory<> ("itemNumber"));
			cContainer.setCellValueFactory(new PropertyValueFactory<> ("container"));
			cGrossWeight.setCellValueFactory(new PropertyValueFactory<> ("grossWeight"));
			cNetWeight.setCellValueFactory(new PropertyValueFactory<> ("netWeight"));
			cCube.setCellValueFactory(new PropertyValueFactory<> ("cube"));
			cTie.setCellValueFactory(new PropertyValueFactory<> ("tie"));
			cHigh.setCellValueFactory(new PropertyValueFactory<> ("high"));
			
			productsTable.setItems(null);
			productsTable.setItems(productList);
		
		}
		catch (SQLException exception)
		{
			exception.printStackTrace();
		}
	}
}
