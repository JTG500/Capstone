package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.InventoryModel;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class InventoryController implements Initializable
{
	
	InventoryModel inventorymodel = new InventoryModel();
	
	@FXML
	private Button closeButton;
	
	@FXML
	private Button addButton;
	
	@FXML
	private Button editButton;
	
	@FXML
	private Button removeButton;
	
	@FXML
	private Button listButton;
	
	@FXML
	private TextField locationField;
	
	@FXML
	private TextField quantityField;
	
	@FXML
	private ComboBox<String> itemBox;
	
	private ObservableList<String> itemList;

	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		try 
		{
			itemBox.getItems().clear();
			itemList = inventorymodel.retrieveItemList();
			itemBox.setItems(itemList);
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void closePressed(ActionEvent event)
	{
		try
		{
			((Node)event.getSource()).getScene().getWindow().hide();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void addPressed(ActionEvent event)
	{
		try
		{
			if (quantityField.getText().equals("") == true)
			{
				quantityField.setText("0");
			}
			
			if (inventorymodel.addInventory(locationField.getText(), Integer.parseInt(quantityField.getText()), itemBox.getValue()) == false)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void editPressed(ActionEvent event)
	{
		try
		{
			if (quantityField.getText().equals("") == true)
			{
				quantityField.setText("0");
			}
			
			if (inventorymodel.editInventory(locationField.getText(), Integer.parseInt(quantityField.getText()), itemBox.getValue()) == true)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void removePressed(ActionEvent event)
	{
		try
		{
			if (quantityField.getText().equals("") == true)
			{
				quantityField.setText("0");
			}
			if (inventorymodel.deleteInventory(locationField.getText(), Integer.parseInt(quantityField.getText()), itemBox.getValue()) == true)
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Success.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
			else
			{
				Stage mainStage = new Stage();
				FXMLLoader loader = new FXMLLoader();
				Pane root = loader.load(getClass().getResource("/FXML/Failure.fxml").openStream());
				Scene scene = new Scene(root);
				mainStage.setScene(scene);
				mainStage.show();
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
	
	public void listPressed(ActionEvent event)
	{
		try
		{
			Stage mainStage = new Stage();
			FXMLLoader loader = new FXMLLoader();
			Pane root = loader.load(getClass().getResource("/FXML/InventoryList.fxml").openStream());
	 		Scene scene = new Scene(root);
	 		mainStage.setScene(scene);
	 		mainStage.show();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}
}
