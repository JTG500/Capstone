package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.InboundListModel;
import Mutators.InboundListMutator;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class InboundListController implements Initializable
{
	
	InboundListModel inboundlistmodel = new InboundListModel();
	
	@FXML
	private TableView<InboundListMutator> inboundTable;
	
	@FXML
	private TableColumn<InboundListMutator, String> cOwner;
	
	@FXML
	private TableColumn<InboundListMutator, String> cCarrier;
	
	@FXML
	private TableColumn<InboundListMutator, String> cShipDate;
	
	@FXML
	private TableColumn<InboundListMutator, Integer> cQuantity;
	
	@FXML
	private TableColumn<InboundListMutator, String> cItemNumber;
	
	@FXML
	private TableColumn<InboundListMutator, String> cTruckNumber;
	
	@FXML
	private TableColumn<InboundListMutator, String> cDriver;
	
	private ObservableList<InboundListMutator> inboundList;
	
	public void initialize(URL location, ResourceBundle resources)
	{
		try
		{
			inboundList = inboundlistmodel.retrieveInboundList();
			cOwner.setCellValueFactory(new PropertyValueFactory<> ("owner"));
			cCarrier.setCellValueFactory(new PropertyValueFactory<> ("carrier"));
			cShipDate.setCellValueFactory(new PropertyValueFactory<> ("shipdate"));
			cQuantity.setCellValueFactory(new PropertyValueFactory<> ("quantity"));
			cTruckNumber.setCellValueFactory(new PropertyValueFactory<> ("truckNumber"));
			cDriver.setCellValueFactory(new PropertyValueFactory<> ("driver"));
			cItemNumber.setCellValueFactory(new PropertyValueFactory<> ("itemNumber"));
			
			inboundTable.setItems(null);
			inboundTable.setItems(inboundList);
		
		}
		catch (SQLException exception)
		{
			exception.printStackTrace();
		}
	}
}
