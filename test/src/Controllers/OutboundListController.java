package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.OutboundListModel;
import Mutators.OutboundListMutator;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class OutboundListController implements Initializable
{
	OutboundListModel outboundlistmodel = new OutboundListModel();
	
	@FXML
	private TableView<OutboundListMutator> outboundTable;
	
	@FXML
	private TableColumn<OutboundListMutator, String> cOwner;
	
	@FXML
	private TableColumn<OutboundListMutator, String> cItemNumber;
	
	@FXML
	private TableColumn<OutboundListMutator, Integer> cQuantity;
	
	@FXML
	private TableColumn<OutboundListMutator, String> cOrderDate;
	
	@FXML
	private TableColumn<OutboundListMutator, String> cShipDate;
	
	@FXML
	private TableColumn<OutboundListMutator, String> cDriver;
	
	@FXML
	private TableColumn<OutboundListMutator, String> cTruckNumber;
	
	private ObservableList<OutboundListMutator> outboundList;
	
	public void initialize(URL location, ResourceBundle resources)
	{
		try
		{
			outboundList = outboundlistmodel.retrieveOutboundList();
			cOwner.setCellValueFactory(new PropertyValueFactory<> ("owner"));
			cItemNumber.setCellValueFactory(new PropertyValueFactory<> ("itemNumber"));
			cQuantity.setCellValueFactory(new PropertyValueFactory<> ("quantity"));
			cOrderDate.setCellValueFactory(new PropertyValueFactory<> ("orderDate"));
			cShipDate.setCellValueFactory(new PropertyValueFactory<> ("shipDate"));
			cDriver.setCellValueFactory(new PropertyValueFactory<> ("driver"));
			cTruckNumber.setCellValueFactory(new PropertyValueFactory<> ("truckNumber"));
			
			outboundTable.setItems(null);
			outboundTable.setItems(outboundList);
		
		}
		catch (SQLException exception)
		{
			exception.printStackTrace();
		}
	}
}

