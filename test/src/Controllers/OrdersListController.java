package Controllers;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import Models.OrdersListModel;
import Mutators.OrdersListMutator;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class OrdersListController implements Initializable
{
	OrdersListModel orderslistmodel = new OrdersListModel();
	
	@FXML
	private TableView<OrdersListMutator> ordersTable;
	
	@FXML
	private TableColumn<OrdersListMutator, String> cName;
	
	@FXML
	private TableColumn<OrdersListMutator, String> cItemNumber;
	
	@FXML
	private TableColumn<OrdersListMutator, Integer> cQuantity;
	
	@FXML
	private TableColumn<OrdersListMutator, String> cOrderDate;
	
	@FXML
	private TableColumn<OrdersListMutator, String> cNeededDate;
	
	@FXML
	private TableColumn<OrdersListMutator, Integer> cOrderID;
	
	private ObservableList<OrdersListMutator> ordersList;
	
	public void initialize(URL location, ResourceBundle resources)
	{
		try
		{
			ordersList = orderslistmodel.retrieveOrdersList();
			cName.setCellValueFactory(new PropertyValueFactory<> ("name"));
			cItemNumber.setCellValueFactory(new PropertyValueFactory<> ("itemNumber"));
			cQuantity.setCellValueFactory(new PropertyValueFactory<> ("quantity"));
			cOrderDate.setCellValueFactory(new PropertyValueFactory<> ("orderDate"));
			cNeededDate.setCellValueFactory(new PropertyValueFactory<> ("neededDate"));
			cOrderID.setCellValueFactory(new PropertyValueFactory<> ("orderID"));
			ordersTable.setItems(null);
			ordersTable.setItems(ordersList);
		
		}
		catch (SQLException exception)
		{
			exception.printStackTrace();
		}
	}	
}