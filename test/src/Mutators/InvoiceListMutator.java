package Mutators;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InvoiceListMutator 
{
	private final IntegerProperty daysStored;
	private final FloatProperty storageCostPerDay;
	private final FloatProperty shippingCost;
	private final StringProperty paymentType;
	private final StringProperty customer;
	private final StringProperty consignee;
	private final StringProperty date;
	
	public InvoiceListMutator(int daysStored, float storageCostPerDay, float shippingCost, String paymentType, String customer, String consignee, String date)
	{
		this.daysStored = new SimpleIntegerProperty(daysStored);
		this.storageCostPerDay = new SimpleFloatProperty(storageCostPerDay);
		this.shippingCost = new SimpleFloatProperty(shippingCost);
		this.paymentType = new SimpleStringProperty(paymentType);
		this.customer = new SimpleStringProperty(customer);
		this.consignee = new SimpleStringProperty(consignee);	
		this.date = new SimpleStringProperty(date);
	}
	
	public int getDaysStored()
	{
		return daysStored.get();
	}
	
	public float getStorageCostPerDay()
	{
		return storageCostPerDay.get();
	}
	
	public float getShippingCost()
	{
		return shippingCost.get();
	}
	
	public String getPaymentType()
	{
		return paymentType.get();
	}
	
	public String getCustomer()
	{
		return customer.get();
	}
	
	public String getConsignee()
	{
		return consignee.get();
	}
	
	public String getDate()
	{
		return date.get();
	}
	
	public void setDaysStored(int value)
	{
		daysStored.set(value);
	}
	
	public void setStorageCostPerDay(float value)
	{
		storageCostPerDay.set(value);
	}
	
	public void setShippingCost(float value)
	{
		shippingCost.set(value);
	}
	
	public void setPaymentType(String value)
	{
		paymentType.set(value);
	}
	
	public void setCustomer(String value)
	{
		customer.set(value);
	}
	
	public void setConsignee(String value)
	{
		consignee.set(value);
	}
	
	public void setDate(String value)
	{
		date.set(value);
	}
	
	public IntegerProperty DaysStoredProperty()
	{
		return daysStored;
	}
	
	public FloatProperty storageCostPerDayProperty()
	{
		return storageCostPerDay;
	}
	
	public FloatProperty shippingCostProperty()
	{
		return shippingCost;
	}
	
	public StringProperty paymentTypeProperty()
	{
		return paymentType;
	}
	
	public StringProperty customerProperty()
	{
		return customer;
	}
	
	public StringProperty consigneeProperty()
	{
		return consignee;
	}
	
	public StringProperty dateProperty()
	{
		return date;
	}
}
