package Mutators;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OutboundListMutator 
{

	private final StringProperty orderDate;
	private final StringProperty shipDate;
	private final StringProperty driver;
	private final StringProperty truckNumber;
	private final StringProperty owner;
	private final StringProperty itemNumber;
	private final IntegerProperty quantity;
	
	public OutboundListMutator(String owner, String itemNumber, int quantity, String orderDate, String shipDate, String driver , String truckNumber)
	{
		this.orderDate = new SimpleStringProperty(orderDate);
		this.driver = new SimpleStringProperty(driver);
		this.shipDate = new SimpleStringProperty(shipDate);
		this.truckNumber = new SimpleStringProperty(truckNumber);
		this.owner = new SimpleStringProperty(owner);
		this.itemNumber = new SimpleStringProperty(itemNumber);
		this.quantity = new SimpleIntegerProperty(quantity);
	}
	
	public String getOrderDate()
	{
		return orderDate.get();
	}
	
	public String getDriver()
	{
		return driver.get();
	}
	
	public String getShipDate()
	{
		return shipDate.get();
	}
	
	public String getTruckNumber()
	{
		return truckNumber.get();
	}
	
	public String getOwner()
	{
		return owner.get();
	}
	
	public String getItemNumber()
	{
		return itemNumber.get();
	}
	
	public int getQuantity()
	{
		return quantity.get();
	}
	
	public void setOrderDate(String value)
	{
		orderDate.set(value);
	}
	
	public void setDriver(String value)
	{
		driver.set(value);
	}
	
	public void setShipDate(String value)
	{
		shipDate.set(value);
	}
	
	public void setTruckNumber(String value)
	{
		truckNumber.set(value);
	}
	
	public void setOwner(String value)
	{
		owner.set(value);
	}
	
	public void setItemNumber(String value)
	{
		itemNumber.set(value);
	}
	
	public void setQuantity(int value)
	{
		quantity.set(value);
	}
	
	public StringProperty orderDateProperty()
	{
		return orderDate;
	}
	
	public StringProperty driverProperty()
	{
		return driver;
	}
	
	public StringProperty shipDateProperty()
	{
		return shipDate;
	}
	
	public StringProperty truckNumberProperty()
	{
		return truckNumber;
	}
	
	public StringProperty ownerProperty()
	{
		return owner;
	}
	
	public StringProperty itemNumberProperty()
	{
		return itemNumber;
	}
	
	public IntegerProperty quantityProperty()
	{
		return quantity;
	}
}

