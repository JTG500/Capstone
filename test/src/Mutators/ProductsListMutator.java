package Mutators;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductsListMutator 
{
	private final StringProperty owner;
	private final StringProperty description;
	private final StringProperty itemNumber;
	private final StringProperty container;
	private final FloatProperty grossWeight;
	private final FloatProperty netWeight;
	private final FloatProperty cube;
	private final FloatProperty tie;
	private final FloatProperty high;
	
	public ProductsListMutator(String owner, String description, String itemNumber, String container, float grossWeight, float netWeight, float cube, float tie, float high)
	{
		this.owner = new SimpleStringProperty(owner);
		this.description = new SimpleStringProperty(description);
		this.itemNumber = new SimpleStringProperty(itemNumber);
		this.container = new SimpleStringProperty(container);
		this.grossWeight = new SimpleFloatProperty(grossWeight);
		this.netWeight = new SimpleFloatProperty(netWeight);
		this.cube = new SimpleFloatProperty(cube);	
		this.tie = new SimpleFloatProperty(tie);
		this.high = new SimpleFloatProperty(high);
	}
	public String getOwner()
	{
		return owner.get();
	}
	
	public String getDescription()
	{
		return description.get();
	}
	
	public String getItemNumber()
	{
		return itemNumber.get();
	}
	
	public String getContainer()
	{
		return container.get();
	}
	
	public float getGrossWeight()
	{
		return grossWeight.get();
	}
	
	public float getNetWeight()
	{
		return netWeight.get();
	}
	
	public float getCube()
	{
		return cube.get();
	}
	
	public float getTie()
	{
		return tie.get();
	}
	
	public float getHigh()
	{
		return high.get();
	}
	
	public void setOwner(String value)
	{
		owner.set(value);
	}
	
	public void setDescription(String value)
	{
		description.set(value);
	}
	
	public void setItemNumber(String value)
	{
		itemNumber.set(value);
	}
	
	public void setContainer(String value)
	{
		container.set(value);
	}
	
	public void setGrossWeight(float value)
	{
		grossWeight.set(value);
	}
	
	public void setNetWeight(float value)
	{
		netWeight.set(value);
	}
	
	public void setCube(float value)
	{
		cube.set(value);
	}
	
	public void setTie(float value)
	{
		tie.set(value);
	}
	
	public void setHigh(float value)
	{
		high.set(value);
	}
	
	public StringProperty ownerProperty()
	{
		return owner;
	}
	
	public StringProperty descriptionProperty()
	{
		return description;
	}
	
	public StringProperty itemNumberProperty()
	{
		return itemNumber;
	}
	
	public StringProperty containerProperty()
	{
		return container;
	}
	
	public FloatProperty grossWeightProperty()
	{
		return grossWeight;
	}
	
	public FloatProperty netWeightProperty()
	{
		return netWeight;
	}
	
	public FloatProperty cubeProperty()
	{
		return cube;
	}
	
	public FloatProperty tieProperty()
	{
		return tie;
	}
	
	public FloatProperty highProperty()
	{
		return high;
	}
}

