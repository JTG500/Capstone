package Mutators;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InventoryListMutator 
{

	private final StringProperty location;
	private final IntegerProperty quantity;
	private final StringProperty itemNumber;
	private final StringProperty shipDate;
	
	public InventoryListMutator(String location, int quantity, String itemNumber, String shipDate)
	{
		this.location = new SimpleStringProperty(location);
		this.quantity = new SimpleIntegerProperty(quantity);
		this.itemNumber = new SimpleStringProperty(itemNumber);
		this.shipDate = new SimpleStringProperty(shipDate);
	}
	
	public String getLocation()
	{
		return location.get();
	}
	
	public int getQuantity()
	{
		return quantity.get();
	}
	
	public String getItemNumber()
	{
		return itemNumber.get();
	}
	
	public String getShipDate()
	{
		return shipDate.get();
	}
	
	public void setLocation(String value)
	{
		location.set(value);
	}
	
	public void setQuantity(int value)
	{
		quantity.set(value);
	}
	
	public void setItemNumber(String value)
	{
		itemNumber.set(value);
	}
	
	public void setShipDate(String value)
	{
		shipDate.set(value);
	}
	
	public StringProperty locationProperty()
	{
		return location;
	}
	
	public IntegerProperty quantityProperty()
	{
		return quantity;
	}
	
	public StringProperty itemNumberProperty()
	{
		return itemNumber;
	}
	
	public StringProperty shipDateProperty()
	{
		return shipDate;
	}	
}


