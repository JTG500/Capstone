package Mutators;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class InboundListMutator 
{
	private final StringProperty owner;
	private final StringProperty carrier;
	private final StringProperty shipdate;
	private final IntegerProperty quantity;
	private final StringProperty truckNumber;
	private final StringProperty driver;
	private final StringProperty itemNumber;
	
	
	public InboundListMutator(String shipper, String carrier, String shipdate, int quantity, String truckNumber, String driver, String itemNumber)
	{
		this.owner = new SimpleStringProperty(shipper);
		this.carrier = new SimpleStringProperty(carrier);
		this.shipdate = new SimpleStringProperty(shipdate);
		this.quantity = new SimpleIntegerProperty(quantity);
		this.truckNumber = new SimpleStringProperty(truckNumber);
		this.driver = new SimpleStringProperty(driver);
		this.itemNumber = new SimpleStringProperty(itemNumber);
		
	}
	
	public String getOwner()
	{
		return owner.get();
	}
	
	public String getCarrier()
	{
		return carrier.get();
	}
	
	public String getShipDate()
	{
		return shipdate.get();
	}
	
	public int getQuantity()
	{
		return quantity.get();
	}
	
	public String getTruckNumber()
	{
		return truckNumber.get();
	}
	
	public String getDriver()
	{
		return driver.get();
	}
	
	public String getItemNumber()
	{
		return itemNumber.get();
	}
	
	public void setOwner(String value)
	{
		owner.set(value);
	}
	
	public void setCarrier(String value)
	{
		carrier.set(value);
	}
	
	public void setShipDate(String value)
	{
		shipdate.set(value);
	}
	
	public void setQuantity(int value)
	{
		quantity.set(value);
	}
	
	public void setTruckNumber(String value)
	{
		truckNumber.set(value);
	}
	
	public void setDriver(String value)
	{
		driver.set(value);
	}
	
	public void setItemNumber(String value)
	{
		itemNumber.set(value);
	}
	
	public StringProperty ownerProperty()
	{
		return owner;
	}
	
	public StringProperty carrierProperty()
	{
		return carrier;
	}
	
	public StringProperty shipdateProperty()
	{
		return shipdate;
	}
	
	public IntegerProperty quantityProperty()
	{
		return quantity;
	}
	
	public StringProperty truckNumberProperty()
	{
		return truckNumber;
	}
	
	public StringProperty driverProperty()
	{
		return driver;
	}
	
	public StringProperty itemNumberProperty()
	{
		return itemNumber;
	}
}

