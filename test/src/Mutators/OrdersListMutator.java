package Mutators;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class OrdersListMutator 
{
	private final StringProperty name;
	private final StringProperty itemNumber;
	private final IntegerProperty quantity;
	private final StringProperty orderDate;
	private final StringProperty neededDate;
	private final IntegerProperty orderID;

	public OrdersListMutator(String name, String itemNumber, int quantity, String orderDate, String neededDate, int orderID)
	{
		this.name = new SimpleStringProperty(name);
		this.itemNumber = new SimpleStringProperty(itemNumber);
		this.quantity = new SimpleIntegerProperty(quantity);
		this.orderDate = new SimpleStringProperty(orderDate);
		this.neededDate = new SimpleStringProperty(neededDate);
		this.orderID = new SimpleIntegerProperty(orderID);		
	}
	
	public String getName()
	{
		return name.get();
	}
	
	public String getItemNumber()
	{
		return itemNumber.get();
	}
	
	public int getQuantity()
	{
		return quantity.get();
	}
	
	public String getOrderDate()
	{
		return orderDate.get();
	}
	
	public String getNeededDate()
	{
		return neededDate.get();
	}
	
	public int getOrderID()
	{
		return orderID.get();
	}
	
	public void setName(String value)
	{
		name.set(value);
	}
	
	public void setItemNumber(String value)
	{
		itemNumber.set(value);
	}
	
	public void setQuantity(int value)
	{
		quantity.set(value);
	}
	
	public void setOrderDate(String value)
	{
		orderDate.set(value);
	}
	
	public void setNeededDate(String value)
	{
		neededDate.set(value);
	}
	
	public void setOrderID(int value)
	{
		orderID.set(value);
	}
	
	public StringProperty nameProperty()
	{
		return name;
	}
	
	public StringProperty itemNumberProperty()
	{
		return itemNumber;
	}
	
	public IntegerProperty quantityProperty()
	{
		return quantity;
	}
	
	public StringProperty orderDateProperty()
	{
		return orderDate;
	}
	
	public StringProperty neededDateProperty()
	{
		return neededDate;
	}
	
	public IntegerProperty orderIDProperty()
	{
		return orderID;
	}
}

