package Mutators;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class CustomerListMutator 
{
	private final StringProperty name;
	private final StringProperty address;
	private final StringProperty city;
	private final StringProperty state;
	private final StringProperty zipcode;
	private final StringProperty country;
	private final StringProperty phone;
	private final StringProperty fax;
	
	public CustomerListMutator(String name, String address, String city, String state, String zipcode, String country, String phone, String fax)
	{
		this.name = new SimpleStringProperty(name);
		this.address = new SimpleStringProperty(address);
		this.city = new SimpleStringProperty(city);
		this.state = new SimpleStringProperty(state);
		this.zipcode = new SimpleStringProperty(zipcode);
		this.country = new SimpleStringProperty(country);
		this.phone = new SimpleStringProperty(phone);
		this.fax = new SimpleStringProperty(fax);		
	}
	
	public String getName()
	{
		return name.get();
	}
	
	public String getAddress()
	{
		return address.get();
	}
	
	public String getCity()
	{
		return city.get();
	}
	
	public String getState()
	{
		return state.get();
	}
	
	public String getZipcode()
	{
		return zipcode.get();
	}
	
	public String getCountry()
	{
		return country.get();
	}
	
	public String getPhone()
	{
		return phone.get();
	}
	
	public String getFax()
	{
		return fax.get();
	}
	
	public void setName(String value)
	{
		name.set(value);
	}
	
	public void setAddress(String value)
	{
		address.set(value);
	}
	
	public void setCity(String value)
	{
		city.set(value);
	}
	
	public void setState(String value)
	{
		state.set(value);
	}
	
	public void setZipcode(String value)
	{
		zipcode.set(value);
	}
	
	public void setCountry(String value)
	{
		country.set(value);
	}
	
	public void setPhone(String value)
	{
		phone.set(value);
	}
	
	public void setFax(String value)
	{
		fax.set(value);
	}
	
	public StringProperty nameProperty()
	{
		return name;
	}
	
	public StringProperty addressProperty()
	{
		return address;
	}
	
	public StringProperty cityProperty()
	{
		return city;
	}
	
	public StringProperty stateProperty()
	{
		return state;
	}
	
	public StringProperty zipcodeProperty()
	{
		return zipcode;
	}
	
	public StringProperty countryProperty()
	{
		return country;
	}
	
	public StringProperty phoneProperty()
	{
		return phone;
	}
	
	public StringProperty faxProperty()
	{
		return fax;
	}
}
